<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('pdf/{idProyect}/{idReleases}', 'PDFController@setID');
Route::get('html', 'RenderController@generarVista');
Route::get('prueba', 'RenderController@obtenerHTML');
