<!DOCTYPE html>
<html lang="en">
  <head>
    <title>{{$data->id}}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <!-- CSS -->
    <style type="text/css">
      table,th{
        text-align: center;
      }
      @page {
        header: page-header;
        footer: page-footer;
      }
    </style>

  </head>
  <body>
    <htmlpageheader name="page-header">
      <p style="font-family: Arial; float: right; width: 28%; font-weight: bold; color: red;">Release deadline: {{ $deadline }}</p>
      <h3 style="float: right; width: 25%; font-weight: bold; font-family: Arial;"> {{ $nameProject }} </h3>
      <p style="font-family: Arial;">Name realease: {{ $nameRelease}}</p>
      <p style="font-family: Arial;">Description: {{ $descriptionProject }}</p>
    </htmlpageheader>

    <h2>Time Line</h2>
      <img src="{{ $uriLineaTiempo }} ">
    <pagebreak>
      <h2>Resources usage</h2>
      <div >
        <img style="float: left; "src="{{ $uriRecurso }} ">
        </div>
        <div>
        <table style=" float: right;"  class="table table-sm">
          <thead class="thead-dark">
            <tr >
              <th>Name</th>
              <th>Used hours</th>
              <th>Skills</th>
            </tr>
          </thead>
          <tbody>
            @foreach($data->resource_usage as $resource)
              <tr>
                <td>{{ $resource->resource_name }}</td>
                <td>{{ $resource->total_used_hours }} out of {{ $resource->total_available_hours }}</td>
                @foreach($resource->skills as $skill )
                <td>{{ $skill->name }}</td>
                @endforeach

              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </pagebreak>

    <pagebreak>
      <h2>Feature dependencies</h2>
      <img src="{{ $uriDependencias }} ">
    </pagebreak>
    <h2>Table view</h2>
          <table style="font-family: Arial;"  class="table table-bordered" >
            <thead class="thead-dark">
              <tr >
                <th>ID</th>
                <th class="table-info">Name</th>
                <th>Effort</th>
                <th>Priority</th>
                <th>Start </th>
                <th>End </th>
                <th>Dependecies </th>
              </tr>
            </thead>
            <tbody>
              @foreach($data->jobs as $job)
                <tr>
                  <td class="table-success">{{ $job->feature->id }} </td>
                  <td class="table-success">{{ $job->feature->name }}</td>
                  <td class="table-success">{{ $job->feature->effort }}</td>
                  <td class="table-success">{{ $job->feature->priority }}</td>
                  <td class="table-success">{{ $job->starts }} <br> </td>
                  <td class="table-success">{{ $job->ends }} <br> </td>
                  @foreach($job->depends_on as $depend)
                    {{$temp = $depend->feature_id}}
                    @if ($temp != $aux)
                      {{ $aux= $temp." ".$aux}}
                    @endif
                  @endforeach
                  <td >{{ $aux   }}</td>

                </tr>
              @endforeach
            </tbody>
          </table>
    <htmlpagefooter name="page-footer">
      <h4 style="font-family: Arial; float: right; width: 58px; font-weight: bold; color: blue;"> {PAGENO}</h4>
    </htmlpagefooter>
  </body>
</html>
