<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class RenderController extends Controller
{
    
	public function generarVista() {
		 
		$data = $this->obtenerDatos()->getData();  
		$jobs = $data->jobs;
		foreach ($jobs as $job) {
			$start = $job->starts;
			$res = explode('T', $start);
			$date = $res[0];
			$time = explode(':', $res[1]);
			$job->starts = $date." ".$time[0].":".$time[1];
			
			$end = $job->ends;
			$res = explode('T', $end);
			$date = $res[0];
			$time = explode(':', $res[1]);
			$job->ends = $date." ".$time[0].":".$time[1];
		}

		$jobs = $data->jobs;

		$vista = \View::make('proyecto', compact('data'));
		$contents = $vista->render(); 
		return view('proyecto',compact('data'));

	}

	public function obtenerDatos() {
		$idProyect = 3;
		$idReleases = 10;

		$client = new Client([ 
	   		'base_uri' => 	'supersede.es.atos.net:8280',
	   ]);

	   $response = $client ->request('GET','/replan/projects/'.$idProyect.'/releases/'.$idReleases.'/plan');

	   $jsonResponse = $response->getBody()->getContents();
	   $data = response()->json( json_decode($jsonResponse)  );
	   return  $data;
	  
	}

    public function obtenerHTML(){

		$client = new Client([ 
	   		'base_uri' => 	'localhost:8000',
	   ]);

	   $response = $client ->request('GET','/html');

	   $jsonResponse = $response->getBody()->getContents();
	   error_log($jsonResponse);

	}

}
