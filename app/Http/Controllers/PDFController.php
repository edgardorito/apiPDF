<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use CpChart\Data;
use CpChart\Image;
use CpChart\Draw;
use CpChart\Chart\Scatter;
use CpChart\Chart\Spring;
use PDF;

class PDFController extends Controller
{
	private $resourceName = array();
	public $idProject = 0;
	public $idRelease = 0;

//Genera el PDF
	public function generarPDF() {
		$pdf = $this->generarVista();
		return $pdf->stream();

//		return $this->generarVista();
	}

//Vista (Carga HTML)
	public function generarVista(){

		//DATA
		$data = $this->obtenerDatos()->getData();
		$dataProject = $this->dataProject()->getData();
		$dataRelease = $this->dataRelease()->getData();

		$this->trabajos($data);
		$uriRecurso = $this->graficaRecursos($data);
		$uriLineaTiempo = $this->graficaLineaTiempo($data,$dataRelease);
		$uriDependencias = $this->graficaDependecias($data);

		//Project
		$nameProject = $dataProject->name;
		$descriptionProject = $dataProject->description;

		//Release
		$nameRelease = $dataRelease->name;
		$deadline = $dataRelease->deadline;
		$deadline = $dataRelease->deadline;
		$deadline = $this->dateDeadline($deadline);

		$names = $this->getNames();

		$view = \View::make('proyecto',compact('data','names','uriRecurso', 'uriLineaTiempo','uriDependencias','nameProject','descriptionProject', 'deadline', 'nameRelease'));
		$pdf = PDF::loadHTML($view,[
  			'format' => 'A4-L',
  			'margin_top'=> 25
  		]);
		return $pdf;

//		return view('proyecto', compact('data','uriRecurso', 'uriLineaTiempo','uriDependencias','nameProject','descriptionProject', 'deadline', 'nameRelease'));

	}

//Graficas
	public function graficaLineaTiempo($data,$dataRelease) {

		$jobs = $data->jobs;

		$startRelease = $dataRelease->starts_at;
		$res = explode('T', $startRelease);
		$date = $res[0];
		$time = explode(':', $res[1]);
		$startRelease = $date;
		$deadline = $this->dateDeadline($dataRelease->deadline);

		$dateStart = new \DateTime($startRelease);
		$dateEnd = new \DateTime($deadline);
		$startRelease = $dateStart->format('m/d');
		$deadline = $dateEnd->format('m/d');

		$duration = array();
		$names = $this->getNames();
		$myPicture = new Image(1480, 500);
		$widthRec = 1479;
		$heightRec = 500;
		$y = 30;
		$x = 200;
		$aux = 300;
		$RectangleSettings = array("R"=>0,"G"=>0,"B"=>0,"Alpha"=>100);
		$RectangleBlue = array("R"=>0,"G"=>0,"B"=>255, "Alpha"=>30, "BorderR"=>0,"BorderG"=>0,"BorderB"=>0);
		$TextSettings = array("R"=>0,"G"=>0,"B"=>0,"Angle"=>0,"FontSize"=>20);
		$TextSettings2 = array("R"=>0,"G"=>0,"B"=>0,"Angle"=>0,"FontSize"=>15);
		$TextSettings3 = array("R"=>0,"G"=>0,"B"=>0,"Angle"=>0,"FontSize"=>12);


		//Cuadro principal
		$i=0;
		$arrayH = array();
		foreach ($names as $name) {
			foreach ($jobs as $job) {
				$nameJob = $job->resource->name;
				if ($name == $nameJob) {
					$myPicture->drawText(5,$y,$nameJob,$TextSettings);
					$myPicture->drawRectangle(0,0,$widthRec-100,$y+10,$RectangleSettings);
					$myPicture->drawLine($aux,0,300,$y+10,array("R"=>0,"G"=>0,"B"=>0,"Weight"=>0));
					$myPicture->drawText(280,80,$startRelease,$TextSettings2);
					$y = $y+50;
					break;
				}
			}

		}
		$y = 10;
		$yAux = 10;
		$aux = 300;
		$count = 0;
		foreach ($names as $name) {
			foreach ($jobs as $job) {
				$nameJob = $job->resource->name;
				$idFeature = $job->feature->id;
				if ($name == $nameJob) {
					$start = new \DateTime($job->starts);
					$end = new \DateTime($job->ends);
					$diff = $end->diff($start);
					$hours = ($diff->days * 24) + $diff->h;
					$xIncremento = $aux+$hours;
					$myPicture->drawFilledRectangle($aux,$y,$xIncremento+40,$y+22,$RectangleBlue);
					$myPicture->drawText($aux+2,$y+20,$idFeature,$TextSettings3);
					$myPicture->drawText($aux+2,$y+20,$idFeature,$TextSettings3);
					$myPicture->drawText(1350,80,$deadline,$TextSettings2);

					$aux = $xIncremento+41;
				}else{
					$aux = 300;
				}
			}
			$yAux = 10;
			$y = $y + 50;
		}

		$myPicture->setGraphArea(20,40,1000,700);
		$myPicture->Render("lineaTiempo.png");
		$uri= $myPicture->toDataURI();
		return $uri;
	}

	public function graficaRecursos($data){
		$resources = $data->resource_usage;
		$datos = new Data();
		$hoursPercent = array();
		$usedHours = array();
		foreach ($resources as $resource) {
			$resourceName[] = $resource->resource_name;
			$availableHours = $resource->total_available_hours;
			$usedHours = $resource->total_used_hours;
			$hoursPercent[] = round(($usedHours * 100) / $availableHours, 2);

		}
		$this->setNames($resourceName);

		$datos->addPoints($hoursPercent, "Porcentaje");
		$datos->setAxisName(0, "Horas usadas");
		$datos->addPoints($resourceName, "Recursos");
		$datos->setSerieDescription("Recursos", "Recursos");
		$datos->setAbscissa("Recursos");

		$image = new Image(600, 300, $datos);

		$image->setFontProperties(["FontName" => "verdana.ttf", "FontSize" => 8]);

		$image->setGraphArea(50, 30, 550, 200);
		$image->drawScale([
		    "Pos" => SCALE_POS_TOPBOTTOM,
		    "Mode"=>SCALE_MODE_ADDALL_START0
		]);
		$Palette = array("0"=>array("R"=>188,"G"=>224,"B"=>46,"Alpha"=>50),
                 "1"=>array("R"=>224,"G"=>100,"B"=>106,"Alpha"=>50),
                 "2"=>array("R"=>224,"G"=>214,"B"=>46,"Alpha"=>50),
                 "3"=>array("R"=>46,"G"=>151,"B"=>224,"Alpha"=>50),
                 "4"=>array("R"=>176,"G"=>46,"B"=>224,"Alpha"=>50),
                 "5"=>array("R"=>224,"G"=>46,"B"=>117,"Alpha"=>50),
                 "6"=>array("R"=>92,"G"=>224,"B"=>46,"Alpha"=>50),
                 "7"=>array("R"=>224,"G"=>176,"B"=>46,"Alpha"=>50));

		//$image->drawProgress(40,60,77,$progressOptions);
		$image->drawBarChart(["DisplayPos" => LABEL_POS_INSIDE, "DisplayValues" => true, "Rounded" => true, "OverrideColors"=>$Palette]);
		//$image->drawLegend(570, 215, ["Style" => LEGEND_NOBORDER, "Mode" => LEGEND_HORIZONTAL]);

		$image->Render("recurso.png");
		$uri= $image->toDataURI();

		return $uri;

	}

	public function graficaDependecias($data){
		$jobs = $data->jobs;

		$myPicture = new Image(300, 300);
		/* Prepare the graph area */
		$myPicture->setGraphArea(20, 20, 280, 280);
		$myPicture->setFontProperties(["FontName" => "Forgotte.ttf", "FontSize" => 9, "R" => 80, "G" => 80, "B" => 80]);
		$myPicture->setShadow(true, ["X" => 2, "Y" => 2, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10]);

		/* Create the pSpring object */
		$springChart = new Spring();

		/* Set the nodes default settings */
		$springChart->setNodeDefaults(["FreeZone" => 50]);

		$featureID = 0;
		$resourceID = 0;
		foreach ($jobs as $job) {
			$connections = [];
			$resourceID = $job->feature->id;
			if ($job->depends_on != null) {
				foreach ($job->depends_on as $depend) {
					$connections[] = $depend->feature_id;
				}
			}
			$springChart->addNode($resourceID, ["Name" => "Node " . $resourceID, "Connections" => $connections ]);
		}

		/* Compute and draw the Spring Graph */
		$springChart->drawSpring($myPicture, ["DrawQuietZone" => true]);

		$myPicture->Render("recurso.png");
		$uri= $myPicture->toDataURI();

		return $uri;
	}
/*
	public function graficaDependecias($data){
		$jobs = $data->jobs;

		$myPicture = new Image(1480, 700);

		$RectangleSettings = array("R"=>0,"G"=>0,"B"=>0,"Alpha"=>100);
		$myPicture->drawRectangle(0,0,1479,600,$RectangleSettings);

		$arrowSettings = array("FillR"=>0,"FillG"=>0,"FillB"=>0);
		$TextSettings = array("R"=>0,"G"=>0,"B"=>0,"Angle"=>0,"FontSize"=>20);
		$x = 45;
		$y = 45;
		$xAux = 150;
		$yAux = 45;
		$arrowX = 0;
		$arrowY = 0;

		$aux= "";
		foreach ($jobs as $job) {
			$idFeaturejob = $job->feature->id;
			if ($job->depends_on != null) {
				$myPicture->drawFilledCircle($x,$y,40,array("R"=>100,"G"=>255,"B"=>218));
				$myPicture->drawText($x-13,$y+5,$idFeaturejob,$TextSettings);
				foreach ($job->depends_on as $depend) {
					$myPicture->drawArrow(85,$x+$arrowX,110,$x+$arrowX,$arrowSettings);
					$nextID = $depend->feature_id;
					if ($idFeaturejob != $aux ) {
						$myPicture->drawFilledCircle($xAux,$y,40,array("R"=>255,"G"=>224,"B"=>130));
						$myPicture->drawText($xAux-13,$y+5,$nextID,$TextSettings);
						$myPicture->drawArrow(190,$x+$arrowX,208,$x+$arrowX,$arrowSettings);
						$y = $y+100;
						$arrowX = $arrowX + 100;
					}elseif ($nextID != $aux) {
						$aux = $idFeaturejob;
						$myPicture->drawFilledCircle($xAux,$yAux,40,array("R"=>255,"G"=>23,"B"=>68));
						$myPicture->drawText($xAux-13,$yAux+5,$nextID,$TextSettings);
						$xAux = $xAux-200;

					}
					$xAux = $xAux+100;
					$yAux = $yAux;
 					$aux = $nextID;
				}
				$x = 45;
			}

		}


		$myPicture->setGraphArea(20,40,1000,700);

		$myPicture->Render("Dependencias.png");
		$uri= $myPicture->toDataURI();
		return $uri;

	}
*/

//Se obtiene la informacion necesaria
	public function obtenerDatos() {
		$idProject = $this->getIdProyect();
		$idRelease = $this->getIdReleases();
		$client = new Client([
	   		'base_uri' => 	'supersede.es.atos.net:8280',
	   ]);

	   $response = $client ->request('GET','/replan/projects/'.$idProject.'/releases/'.$idRelease.'/plan');

	   $jsonResponse = $response->getBody()->getContents();
	   $data = response()->json( json_decode($jsonResponse)  );

	   return  $data;

	}

	public function dataProject() {
		$idProject = $this->getIdProyect();
		$client = new Client([
	   		'base_uri' => 	'supersede.es.atos.net:8280',
	   ]);

	   $response = $client ->request('GET','/replan/projects/'.$idProject);

	   $jsonResponse = $response->getBody()->getContents();
	   $data = response()->json( json_decode($jsonResponse)  );

	   return  $data;
	}

	public function dataRelease() {
		$idProject = $this->getIdProyect();
		$idRelease = $this->getIdReleases();

		$client = new Client([
	   		'base_uri' => 	'supersede.es.atos.net:8280',
	   ]);

	   $response = $client ->request('GET','/replan/projects/'.$idProject.'/releases/'.$idRelease);

	   $jsonResponse = $response->getBody()->getContents();
	   $data = response()->json( json_decode($jsonResponse)  );

	   return  $data;
	}

	//Se transforma la fecha del deadline en un formato entendible
	public function dateDeadline($deadline){
		$res = explode('T', $deadline);
		$date = $res[0];
		$time = explode(':', $res[1]);
		$deadline = $date." ".$time[0].":".$time[1];
		return  $deadline;
	}

	//Se obtiene los jobs del plan
	public function trabajos($data){
		$jobs = $data->jobs;

		foreach ($jobs as $job) {
			$start = $job->starts;
			$res = explode('T', $start);
			$date = $res[0];
			$time = explode(':', $res[1]);
			$job->starts = $date." ".$time[0].":".$time[1];

			$end = $job->ends;
			$res = explode('T', $end);
			$date = $res[0];
			$time = explode(':', $res[1]);
			$job->ends = $date." ".$time[0].":".$time[1];

		}

		return $jobs;
	}

  //Resource Names
  public function getNames(){ return $this->resourceName;}
  public function setNames($resourceName){$this->resourceName = $resourceName;}

    //Id
  public function setID($pidProject,$pidReleases){
  	$this->idProject = $pidProject;
  	$this->idRelease = $pidReleases;
		return $this->generarPDF();
  }

  public function getIdProyect(){ return $this->idProject;}
  public function getIdReleases(){ return $this->idRelease;}


};
